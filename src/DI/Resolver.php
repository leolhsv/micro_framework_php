<?php


namespace LeoVales\DI;

class Resolver
{
    private $dependencies;
    public function method($method, array $dependencies = [])
    {
        $this->dependencies = $dependencies;
        $info = new \ReflectionFunction($method);
        $parameters = $info->getParameters();
        $parameters = $this->resolveParameters($parameters);
        return call_user_func_array($info->getClosure(), $parameters);
    }

    public function class($class, array $dependencies = [])
    {
        $this->dependencies = $dependencies;
        $class = new \ReflectionClass($class);
        if (!$class->isInstantiable()) {
            throw new \Exception("{$class} is not instantiable");
        }
        $constructor = $class->getConstructor();
        if (!$constructor) {
            return new $class->name;
        }
        $parameters = $constructor->getParameters();
        $parameters = $this->resolveParameters($parameters);
        return $class->newInstanceArgs($parameters);
    }

    private function resolveParameters($parameters)
    {
        $dependencies = [];
        foreach ($parameters as $parameter) {
            $dependency = $parameter->getClass();
            if ($dependency) {
                $dependencies[] = $this->class($dependency->name, $this->dependencies);
            } else {
                $dependencies[] = $this->getDependencies($parameter);
            }
        }
        return $dependencies;
    }

    private function getDependencies($parameter)
    {
        if (isset($this->dependencies[$parameter->name])) {
            return $this->dependencies[$parameter->name];
        }
        
        if ($parameter->isDefaultValueAvailable()) {
            return $parameter->getDefaultValue();
        }
        throw new \Exception("{parameter->name} not receive a valid value");
    }
}




























// class Resolver
// {
// 	private $dependencias;

// 	public function method($method, array $dependencias = [])
// 	{
// 		$this->dependencias = $dependencias;

// 		$info = new \ReflectionFunction($method);
// 		$parametros = $info->getParameters();
// 		$parametros = $this->resolveParameters($parametros);

// 		return call_user_func_array($info->getClosure, $parametros);
// 	}

// 	public function class($class, array $dependencias = [])
// 	{
// 		$this->dependencias = $dependencias;

// 		$class = new \ReflectionClass($class);

// 		if (!$class->IsInstatiable()) {
// 			throw new \Exception("{$class} não é instanciável");
// 		}

// 		$constructor = $class->getConstructor();

// 		if (!$constructor) {
// 			return new $class->name;
// 		}

// 		$parametros = $constructor->getParameters();
// 		$parametros = $this->resolveParameters($parametros);

// 		return $class->newInstanceArgs($parametros);
// 	}	

// 	private function resolveParameters($parametros)
// 	{
// 		$dependencias[];

// 		foreach ($parametros as $parametro) {
// 			$dependencia = $parametro->getClass();

// 			if ($dependencia) {
// 				$dependencias[] = $this->class($dependencia->name, $this->dependencias);
// 			} else {
// 				$dependencias[] = $this->getDependencias($parametro);
// 			}
// 		}

// 		return $dependencias;
// 	}

// 	private function getDependencias($parametro)
// 	{
// 		if (isset($this->dependencias[$parametro->name])) {
// 			return $this->dependencias[$parametro->name];
// 		}

// 		if ($parametro->isDefaultValueAvailable()) {
// 			return $parametro->getDefaultValue();
// 		}

// 		throw new \Exception("{$parametro->name} não é válido");
// 	}
// }
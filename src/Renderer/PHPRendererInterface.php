<?php


namespace LeoVales\Renderer;

interface PHPRendererInterface
{
    public function setData($data);
    public function run();
}